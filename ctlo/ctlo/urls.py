from django.contrib import admin
from django.urls import re_path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    # re_path('^blog/', include('blog.urls')),
    re_path('^ppe/', include('blog.urls')),
    re_path('^admin/', admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
