from django.urls import re_path

from . import views

app_name = 'blog'

urlpatterns = [
    re_path('^$', views.index, name='index'),
    re_path('^login/', views.login_view),
    re_path('^signup/', views.register_view),
    re_path('^logout/', views.logout_view),
]
