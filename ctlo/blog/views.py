from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from .models import Question

from django.contrib.auth import (
    authenticate,
    get_user_model,
    login,
    logout
)

from .forms import UserLoginForm, UserRegisterForm


@login_required
def index(request):

    if request.POST.get('newQuestion') != None:
        
        qId = request.POST.get('qId')
        q = get_object_or_404(Question, pk=qId)
        q.questionText = request.POST.get('newQuestion')
        
        submit = request.POST.get('submit')

        if 'Enregistrer' in submit:

            q.choice_A = request.POST.get('choiceA{}'.format(qId))
            q.choice_B = request.POST.get('choiceB{}'.format(qId))
            q.choice_C = request.POST.get('choiceC{}'.format(qId))
            q.choice_D = request.POST.get('choiceD{}'.format(qId))

            q.answer = request.POST.get('radAnswer')

            q.save()
        elif 'Annuler' in submit:
            questions = Question.objects.all().order_by('-id')
            return render(request, 'blog/index.html', {'questions': questions})

        else:
            q.delete()
    elif request.POST.get('verifAjouter') != None:
        Question.objects.create()
    questions = Question.objects.all().order_by('-id')
    return render(request, 'blog/index.html', {'questions': questions})


# def index(request):
    # return render(request, 'blog/index.html', {})

def login_view(request):

    next = request.GET.get('next')
    form = UserLoginForm(request.POST or None)

    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        login(request, user)

        if next:
            return redirect(next)
        return redirect('/ppe')
    
    context = {
        'form': form,
    }

    logout(request)
    return render(request, 'blog/login.html', context)

def register_view(request):

    next = request.GET.get('next')
    form = UserRegisterForm(request.POST or None)

    form.fields['username'].help_text = False
    form.fields['username'].label = 'Nom d\'utilisateur (prénom.nom)'

    if form.is_valid():
        user = form.save(commit=False)
        password = form.cleaned_data.get('password')
        user.set_password(password)
        user.first_name = user.username.split('.')[0].capitalize()
        user.last_name = user.username.split('.')[1].upper()
        user.save()
        new_user = authenticate(username=user.username, password=password)
        login(request, new_user)

        if next:
            return redirect(next)
        return redirect('/ppe', user)
    
    context = {
        'form': form,
    }

    logout(request)
    return render(request, 'blog/signup.html', context)

def logout_view(request):
    logout(request)
    return redirect('/ppe/login')

