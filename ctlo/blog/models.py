from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    isTeacher = models.BooleanField(default=False)

    @receiver
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()

    def __str__(self):
        return self.user.username



class Class(models.Model):

    title = models.CharField(max_length=64)
    students = models.ManyToManyField(Profile, limit_choices_to={'isTeacher': False})

    def __str__(self):
        return self.title


class Question(models.Model):

    questionText = models.CharField(max_length=256, default='')

    choice_A = models.CharField(max_length=256, default='')
    choice_B = models.CharField(max_length=256, default='')
    choice_C = models.CharField(blank=True, max_length=256, default='')
    choice_D = models.CharField(blank=True, max_length=256, default='')

    answer = models.CharField(max_length=1, default='A')

    def __str__(self):
        return self.questionText
    


