from django import forms

from django.contrib.auth import (
    authenticate,
    get_user_model
)

User = get_user_model()

class UserLoginForm(forms.Form):

    username = forms.CharField(help_text=False, label='Nom d\'utilisateur (prénom.nom)')
    password = forms.CharField(widget=forms.PasswordInput, label='Mot de passe') 

    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:

            user = authenticate(username=username, password=password)

            if not user:
                raise forms.ValidationError('Nom d\'utilisateur ou mot de passe incorrect.')

        return super(UserLoginForm, self).clean(*args, **kwargs)

class UserRegisterForm(forms.ModelForm):

    email = forms.EmailField(label='Adresse email')
    password = forms.CharField(widget=forms.PasswordInput, label='Mot de passe')

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password'
        ]

    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get('username')
        email = self.cleaned_data.get('email')

        email_qs = User.objects.filter(email=email) # email query set
        if email_qs.exists():
            raise forms.ValidationError('Cette adresse email est déjà utilisée.')

        username_qs = User.objects.filter(username=username) # username query set
        if username_qs.exists():
            raise forms.ValidationError('Ce nom d\'utilisateur est déjà utilisé. Cliquez ci-dessous pour vous connecter.')

        return super(UserRegisterForm, self).clean(*args, **kwargs)
        
