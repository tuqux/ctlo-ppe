from django.contrib import admin

from .models import Class, Question, Profile

admin.site.register(Class)
admin.site.register(Question)
admin.site.register(Profile)
